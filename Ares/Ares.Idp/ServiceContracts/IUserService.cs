﻿using Ares.Idp.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ares.Idp.ServiceContracts
{
    public interface IUserService : IBaseService
    {
        Task<List<UserDetailDto>> GetAll();
        Task<UserDetailDto> GetById(string id);
        Task<bool> CheckUserName(string userName);
        Task<ResultDto> Create(UserDto user);
        Task<ResultDto> Edit(UserDto user);
        Task<ResultDto> Delete(string id);
    }
}
