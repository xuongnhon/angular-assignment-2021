﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace Ares.Idp
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResource {
                    Name = "Demeter.Api"
                }
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new List<ApiResource>
            {
                new ApiResource {
                    Name = "Demeter.Api",
                    DisplayName = "Demeter.Api",
                    UserClaims = { "role" },
                    ApiSecrets = { new Secret("e579879b-ad1f-4e6f-a73e-0eb452b6819c".Sha256()) },
                    Scopes = {
                        "Demeter.Api"
                    }
               }
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "ArtemisClient",
                    RequireClientSecret = false,

                    AllowedGrantTypes = GrantTypes.Code,
                    AccessTokenType = AccessTokenType.Reference,

                    RedirectUris = {
                        "http://localhost:4200/signin-callback",
                        "http://localhost:4200/silent"
                    },
                    PostLogoutRedirectUris = { "http://localhost:4200/signout-callback" },

                    AllowOfflineAccess = true,
                    AllowedScopes = { "openid", "profile", "Demeter.Api" },
                }
            };
    }
}
