﻿using Ares.Idp.Common.Constants;
using Ares.Idp.Dto;
using Ares.Idp.ServiceContracts;
using IdentityModel;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ares.Idp.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly Dictionary<string, int> _roles = new Dictionary<string, int> {
            { "admin", 1 },
            { "user", 2 },
        };

        public UserService(
            UserManager<IdentityUser> userManager
        )
        {
            _userManager = userManager;
        }

        public async Task<List<UserDetailDto>> GetAll()
        {
            var userDetailDtos = new List<UserDetailDto>();

            foreach (var user in _userManager.Users.ToList())
            {
                var userRole = (await _userManager.GetRolesAsync(user)).First();
                var userClaims = await _userManager.GetClaimsAsync(user);

                var userDetailDto = new UserDetailDto
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Name = userClaims.First(x => x.Type == "name").Value,
                    RoleId = GetRoleId(userRole),
                    RoleName = userRole
                };

                userDetailDtos.Add(userDetailDto);
            }

            return userDetailDtos;
        }

        public async Task<UserDetailDto> GetById(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user != null)
            {
                var userRole = (await _userManager.GetRolesAsync(user)).First();
                var userClaims = await _userManager.GetClaimsAsync(user);

                var userDetailDto = new UserDetailDto
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Name = userClaims.First(x => x.Type == "name").Value,
                    RoleId = GetRoleId(userRole),
                    RoleName = userRole
                };

                return userDetailDto;
            }

            throw new Exception(Message.UserNotFound);
        }

        public async Task<bool> CheckUserName(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);

            return user != null;
        }

        public async Task<ResultDto> Create(UserDto user)
        {
            var newUser = await _userManager.FindByNameAsync(user.UserName);
            if (newUser == null)
            {
                newUser = new IdentityUser
                {
                    UserName = user.UserName
                };

                var result = await _userManager.CreateAsync(newUser, user.Password);
                if (!result.Succeeded)
                {
                    return new ResultDto
                    {
                        Status = Common.Enum.ActionStatus.Error,
                        Message = result.Errors.First().Description
                    };
                }

                result = await _userManager.AddClaimsAsync(newUser, new Claim[]{
                    new Claim(JwtClaimTypes.Name, user.Name),
                });
                if (!result.Succeeded)
                {
                    return new ResultDto
                    {
                        Status = Common.Enum.ActionStatus.Error,
                        Message = result.Errors.First().Description
                    };
                }

                result = await _userManager.AddToRoleAsync(newUser, GetRoleName(user.RoleId));
                if (!result.Succeeded)
                {
                    return new ResultDto
                    {
                        Status = Common.Enum.ActionStatus.Error,
                        Message = result.Errors.First().Description
                    };
                }

                return new ResultDto
                {
                    Status = Common.Enum.ActionStatus.Success,
                    Data = newUser.Id
                };
            }

            throw new Exception(Message.UserNameExist);
        }

        public async Task<ResultDto> Edit(UserDto user)
        {
            var dbUser = await _userManager.FindByIdAsync(user.Id);
            if (dbUser != null)
            {
                var userRole = (await _userManager.GetRolesAsync(dbUser)).First();
                var userClaims = await _userManager.GetClaimsAsync(dbUser);
                var nameClaim = userClaims.First(x => x.Type == "name");

                await _userManager.RemoveFromRoleAsync(dbUser, userRole);
                await _userManager.RemoveClaimAsync(dbUser, nameClaim);

                await _userManager.AddToRoleAsync(dbUser, GetRoleName(user.RoleId));
                await _userManager.AddClaimAsync(dbUser, new Claim(JwtClaimTypes.Name, user.Name));

                return new ResultDto
                {
                    Status = Common.Enum.ActionStatus.Success
                };
            }

            throw new Exception(Message.UserNotFound);
        }

        public async Task<ResultDto> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                if (user.UserName == "admin")
                {
                    return new ResultDto
                    {
                        Status = Common.Enum.ActionStatus.Error
                    };
                }

                await _userManager.DeleteAsync(user);

                return new ResultDto
                {
                    Status = Common.Enum.ActionStatus.Success
                };
            }

            throw new Exception(Message.UserNotFound);
        }

        private int GetRoleId(string roleName)
        {
            return _roles[roleName];
        }

        private string GetRoleName(int roleId)
        {
            return _roles.First(x => x.Value == roleId).Key;
        }
    }
}
