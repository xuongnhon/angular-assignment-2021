﻿namespace Ares.Idp.Models
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
    }
}
