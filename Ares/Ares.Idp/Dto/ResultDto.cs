﻿using Ares.Idp.Common.Enum;

namespace Ares.Idp.Dto
{
    public class ResultDto
    {
        public ActionStatus Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
