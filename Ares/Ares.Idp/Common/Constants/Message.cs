﻿namespace Ares.Idp.Common.Constants
{
    public static class Message
    {
        public const string CreateUserSuccessfully = "Create user successfully!";
        public const string EditUserSuccessfully = "Edit user successfully!";
        public const string DeleteUserSuccessfully = "Delete user successfully!";
        public const string UserNotFound = "User could not be found!";
        public const string UserNameExist = "The username exist!";
        public const string UserNameLoggedIn = "Can not delete this user, because user is currently logged in!";
    }
}
