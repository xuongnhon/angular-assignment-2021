﻿using Ares.Idp.Data;
using IdentityModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Ares.Idp
{
    public static class SeedData
    {
        public static async Task EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();
            services.AddLogging();
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(connectionString));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    context.Database.Migrate();

                    var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                    await CreateRoles(roleManager);

                    var userManager = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();

                    await CreateUsers(userManager);
                }
            }
        }

        private static async Task CreateRoles(RoleManager<IdentityRole> roleManager)
        {
            var adminRole = await roleManager.FindByNameAsync("admin");
            if (adminRole == null)
            {
                await roleManager.CreateAsync(new IdentityRole
                {
                    Name = "admin"
                });
            }

            var userRole = await roleManager.FindByNameAsync("user");
            if (userRole == null)
            {
                await roleManager.CreateAsync(new IdentityRole
                {
                    Name = "user"
                });
            }
        }

        private static async Task CreateUsers(UserManager<IdentityUser> userManager)
        {
            var admin = await userManager.FindByNameAsync("admin");
            if (admin == null)
            {
                admin = new IdentityUser
                {
                    UserName = "admin",
                };

                await userManager.CreateAsync(admin, "P@ssw0rd");

                await userManager.AddClaimsAsync(admin, new Claim[]{
                    new Claim(JwtClaimTypes.Name, "Nhon Vuong"),
                });

                await userManager.AddToRoleAsync(admin, "admin");
            }
        }
    }
}
