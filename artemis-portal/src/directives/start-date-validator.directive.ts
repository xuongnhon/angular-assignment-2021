import { Directive, Input } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl, ValidationErrors } from '@angular/forms';

@Directive({
    selector: '[startDateValidator]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: StartDateValidatorDirective,
        multi: true
    }]
})
export class StartDateValidatorDirective implements Validator {
    @Input('startDate') startDate: string;
    @Input('endDate') endDate: string;

    validate(form: AbstractControl): ValidationErrors | null {
        let startDateControl = form.get(this.startDate);
        let endDateControl = form.get(this.endDate);

        if (startDateControl && endDateControl) {
            let startDateValue = startDateControl.value;
            let endDateValue = endDateControl.value;

            if (startDateValue && endDateValue
                && startDateValue > endDateValue) {
                startDateControl.setErrors({ greaterThanEndDate: true });
                return null;
            }

            if (startDateControl.errors) {
                delete startDateControl.errors['greaterThanEndDate'];

                if (Object.keys(startDateControl.errors).length === 0) {
                    startDateControl.setErrors(null);
                }
            }
        }

        return null;
    }
}