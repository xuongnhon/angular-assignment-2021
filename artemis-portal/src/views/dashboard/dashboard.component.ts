import { Component, OnInit } from '@angular/core';
import AuthService from 'src/services/auth-service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  name: string | undefined;

  constructor(private authService: AuthService) {
  }

  async ngOnInit() {
    let loggedUser = await this.authService.getUser();
    this.name = loggedUser?.profile?.name;
  }
}
