import { TestBed } from '@angular/core/testing';
import AuthService from 'src/services/auth-service';
import { RouterTestingModule } from '@angular/router/testing';
import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent class testing', () => {
  let component: DashboardComponent;
  let authService: AuthService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [
        DashboardComponent,
        { provide: AuthService, useClass: MockAuthService }
      ]
    });
    component = TestBed.inject(DashboardComponent);
    authService = TestBed.inject(AuthService);
  });

  it('Name should have value', async () => {
    await component.ngOnInit();
    expect(component.name).toBe('Nhon');
  });

  it('Name should be undefined', async () => {
    authService.getUser = () => {
      return new Promise((resolve) => {
        resolve(null);
      });
    };
    await component.ngOnInit();
    expect(component.name).toBeUndefined();
  });
});

describe('DashboardComponent DOM testing', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        DashboardComponent
      ],
      providers: [
        { provide: AuthService, useClass: MockAuthService }
      ]
    }).compileComponents();
  });

  it('Should render a welcome message', async () => {
    const welcomeMessage = 'Welcome Nhon'
    const fixture = TestBed.createComponent(DashboardComponent);
    const component = fixture.componentInstance;
    await component.ngOnInit();
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('div.alert.alert-success')?.textContent).toContain(welcomeMessage);
  });

  it('Should have a close button', () => {
    const fixture = TestBed.createComponent(DashboardComponent);
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('div.alert.alert-success button')).toBeTruthy();
  });
});

class MockAuthService {
  getUser() {
    return new Promise((resolve) => {
      resolve({
        profile: {
          name: 'Nhon'
        }
      });
    });
  }
}