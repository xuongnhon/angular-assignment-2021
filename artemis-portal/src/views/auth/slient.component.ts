import { AfterViewInit, Component } from '@angular/core';
import AuthService from '../../services/auth-service';

@Component({
  selector: 'slient',
  template: ''
})
export class SlientComponent implements AfterViewInit {
  constructor(private authService: AuthService) {
  }

  async ngAfterViewInit() {
    await this.authService.signinSilentCallback();
  }
}
