import { AfterViewInit, Component } from '@angular/core';
import { Router } from '@angular/router'
import AuthService from '../../services/auth-service';

@Component({
  selector: 'signout-callback',
  template: '<loading></loading>'
})
export class SignoutCallbackComponent implements AfterViewInit {
  constructor(private router: Router, private authService: AuthService) {
  }

  async ngAfterViewInit() {
    await this.authService.signoutRedirectCallback();

    this.router.navigate(['/logout']);
  }
}
