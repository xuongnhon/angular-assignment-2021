import { AfterViewInit, Component } from '@angular/core';
import { Router } from '@angular/router'
import AuthService from '../../services/auth-service';

@Component({
  selector: 'signin-callback',
  template: '<loading></loading>'
})
export class SigninCallbackComponent implements AfterViewInit {
  constructor(private router: Router, private authService: AuthService) {
  }

  async ngAfterViewInit() {
    var user = await this.authService.signinRedirectCallback();

    if (user) {
      this.router.navigate(['/dashboard']);
    }
  }
}
