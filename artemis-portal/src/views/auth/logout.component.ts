import { OnInit, Component } from '@angular/core';
import AuthService from "../../services/auth-service";

@Component({
  selector: 'logout',
  templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit {
  constructor(private authService: AuthService) {
  }

  async ngOnInit() {
    let isAuthenticated = await this.authService.isAuthenticated();

    if (isAuthenticated) {
      await this.authService.logout();
    }
  }

  login() {
    this.authService.login();
  }
}
