import { OnInit, Component } from '@angular/core';
import { Router } from '@angular/router'
import AuthService from "../../services/auth-service";

@Component({
  selector: 'login',
  template: ''
})
export class LoginComponent implements OnInit {
  constructor(private router: Router, private authService: AuthService) {
  }

  async ngOnInit() {
    let isAuthenticated = await this.authService.isAuthenticated();

    if (isAuthenticated) {
      this.router.navigate(['/dashboard']);
    } else {
      this.authService.login();
    }
  }
}
