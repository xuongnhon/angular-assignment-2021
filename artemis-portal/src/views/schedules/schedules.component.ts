import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Schedule } from 'src/models/schedule';
import ScheduleService from 'src/services/schedule-service';

@Component({
  selector: 'schedules',
  templateUrl: './schedules.component.html'
})
export class SchedulesComponent implements OnInit {
  loading: boolean = true;
  schedules: Schedule[];
  selectedSchedule: Schedule;
  showDialog: boolean;

  constructor(private router: Router, private scheduleService: ScheduleService) {
  }

  async ngOnInit() {
    this.schedules = await this.scheduleService.getAll();

    this.loading = false;
  }

  trackById(index: number, schedule: Schedule) {
    return schedule.id;
  }

  showDetail(schedule: Schedule) {
    this.selectedSchedule = schedule;

    this.showDialog = true;
  }

  closeDialog() {
    this.showDialog = false;
  }

  editSchedule(schedule: Schedule) {
    this.router.navigate([`/schedules/edit/${schedule.id}`]);
  }

  async delete(schedule: Schedule) {
    const continute = confirm('Are you sure you want to delete?');

    if (continute) {
      this.loading = true;

      await this.scheduleService.delete(schedule.id);

      this.schedules = await this.scheduleService.getAll();

      this.loading = false;
    }
  }
}
