import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { NgForm } from '@angular/forms';
import { User } from 'src/models/user';
import userService from 'src/services/user-service';
import ScheduleService from 'src/services/schedule-service';
import { Schedule } from 'src/models/schedule';
import { IDeactivateComponent } from 'src/guards/deactivate.guard';

@Component({
  selector: 'create-user-schedule',
  templateUrl: './create-user-schedule.component.html'
})
export class CreateUserScheduleComponent implements OnInit, IDeactivateComponent {
  loading: boolean = true;
  user: User;
  schedule: Schedule = {
    id: 0,
    title: '',
    creator: {
      id: 0,
      name: '',
      role: ''
    },
    description: '',
    location: '',
    startDate: null,
    endDate: null,
  };

  skipDirtyCheck: boolean = false;
  @ViewChild('scheduleForm') scheduleForm: NgForm;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: userService,
    private scheduleService: ScheduleService
  ) {
  }

  async ngOnInit() {
    const userId = Number(this.activatedRoute.snapshot.paramMap.get('userId'));
    this.user = await this.userService.getById(userId);
    this.schedule.creator = { ...this.user };

    this.loading = false;
  }

  canExit() {
    if (!this.skipDirtyCheck && (this.scheduleForm.touched || this.scheduleForm.dirty)) {
      return confirm("There is unsaved data on this page. Do you wish to continute?");
    }

    return true;
  }

  back() {
    this.router.navigate(['/users']);
  }

  async onSubmit() {
    if (this.scheduleForm.valid) {
      await this.scheduleService.add(this.schedule);

      this.skipDirtyCheck = true;
      this.router.navigate(['/schedules']);
    }
  }
}
