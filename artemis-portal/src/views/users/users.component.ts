import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { IDeactivateComponent } from 'src/guards/deactivate.guard';
import { User } from 'src/models/user';
import UserService from 'src/services/user-service';
import { UserDetailComponent } from './user-detail.component';

@Component({
  selector: 'users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit, IDeactivateComponent {
  loading: boolean = true;
  users: User[];

  @ViewChildren('userDetails') userDetailComponents: QueryList<UserDetailComponent>;

  constructor(private router: Router, private userService: UserService) {
  }

  async ngOnInit() {
    this.users = await this.userService.getAll();

    this.loading = false;
  }

  skipDirtyCheck: boolean = false;
  canExit() {
    if (!this.skipDirtyCheck) {
      const userDetailComponent = this.userDetailComponents
        .toArray()
        .find(x => x.userForm.dirty || x.userForm.touched);
      if (userDetailComponent != null) {
        return confirm("There is unsaved data on this page. Do you wish to continute?");
      }
    }

    return true;
  }

  trackById(index: number, user: User) {
    return user.id;
  }

  async reload() {
    this.loading = true;

    this.users = await this.userService.getAll();

    this.loading = false;
  }

  addNewUser() {
    this.router.navigate(['/users/create']);
  }
}
