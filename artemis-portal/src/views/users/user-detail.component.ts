import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/models/user';
import UserService from 'src/services/user-service';

@Component({
  selector: 'user-detail',
  templateUrl: './user-detail.component.html'
})
export class UserDetailComponent {
  @Input() user: User;
  @Output() editUserSuccessEvent = new EventEmitter();
  @Output() deleteUserSuccessEvent = new EventEmitter();

  editUser: User;
  editMode: boolean;

  @ViewChild('userForm') userForm: NgForm;

  constructor(private router: Router, private userService: UserService) {
  }

  switchToEditMode() {
    this.editUser = { ...this.user };
    this.editMode = true;
  }

  switchToViewMode() {
    this.userForm.reset();
    this.editMode = false;
  }

  createSchedule() {
    this.router.navigate([`/users/${this.user.id}/create-schedule`]);
  }

  haveAnyChanges() {
    return this.user.name !== this.editUser.name
      || this.user.role !== this.editUser.role;
  }

  async delete() {
    const continute = confirm(`Are you sure you want to delete user "${this.user.name}"?`);

    if (continute) {
      await this.userService.delete(this.user.id);
      this.deleteUserSuccessEvent.emit();
    }
  }

  async onSubmit() {
    if (this.haveAnyChanges() && this.userForm.valid) {
      await this.userService.edit(this.editUser);

      this.editUserSuccessEvent.emit();
    }

    this.switchToViewMode();
  }
}
