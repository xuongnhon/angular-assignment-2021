import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router'
import { NgForm } from '@angular/forms';
import { User } from 'src/models/user';
import userService from 'src/services/user-service';
import { IDeactivateComponent } from 'src/guards/deactivate.guard';

@Component({
  selector: 'create-user',
  templateUrl: './create-user.component.html'
})
export class CreateUserComponent implements IDeactivateComponent {
  user: User;

  skipDirtyCheck: boolean = false;
  @ViewChild('userForm') userForm: NgForm;

  constructor(private router: Router, private userService: userService) {
    this.user = {
      id: 0,
      name: '',
      role: ''
    }
  }

  canExit() {
    if (!this.skipDirtyCheck && (this.userForm.touched || this.userForm.dirty)) {
      return confirm("There is unsaved data on this page. Do you wish to continute?");
    }

    return true;
  }

  back() {
    this.router.navigate(['/users']);
  }

  async onSubmit() {
    if (this.userForm.valid) {
      await this.userService.add(this.user);

      this.skipDirtyCheck = true;
      this.router.navigate(['/users']);
    }
  }
}
