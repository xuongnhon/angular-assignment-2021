import { OnInit, Component } from '@angular/core';
import AuthService from "../../services/auth-service";

@Component({
  selector: 'internal-layout',
  templateUrl: './internal-layout.component.html'
})
export class InternalLayoutComponent implements OnInit {
  navigationItems: NavigationItem[] = [
    new NavigationItem("Dashboard", "/dashboard"),
    new NavigationItem("Task", "/tasks"),
    new NavigationItem("User", "/users"),
    new NavigationItem("Schedule", "/schedules")
  ];

  user: any = {};

  constructor(private authService: AuthService) {

  }

  async ngOnInit() {
    this.user = await this.authService.getUser();
  }

  async logout() {
    await this.authService.logout();
  }
}

class NavigationItem {
  name: string;
  path: string;

  constructor(name: string, path: string) {
    this.name = name;
    this.path = path;
  }
}
