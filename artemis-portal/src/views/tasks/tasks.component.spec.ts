import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { TasksComponent } from './tasks.component';
import { LoadingComponent } from 'src/components/loading/loading.component';
import TaskService from 'src/services/task-service';
import { TaskStatus } from 'src/enums/task-status';

describe('TasksComponent class testing', () => {
  let component: TasksComponent;
  let router: Router;
  let taskService: TaskService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        TasksComponent,
        { provide: TaskService, useClass: MockTaskService }
      ]
    });
    component = TestBed.inject(TasksComponent);
    router = TestBed.inject(Router);
    taskService = TestBed.inject(TaskService);
  });

  it('Should get tasks', async () => {
    await component.ngOnInit();
    expect(component.tasks.length).toBe(tasks.length);
  });

  it('All task lists should be correct', async () => {
    await component.ngOnInit();
    expect(component.newTasks.length).toBe(1);
    expect(component.inProgressTasks.length).toBe(2);
    expect(component.doneTasks.length).toBe(1);
  });
});

describe('TasksComponent DOM testing', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      declarations: [
        TasksComponent,
        LoadingComponent
      ],
      providers: [
        { provide: TaskService, useClass: MockTaskService }
      ]
    }).compileComponents();
  });

  it('Should show loading first', () => {
    const fixture = TestBed.createComponent(TasksComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('div.modal')).toBeTruthy();
  });

  it('Should contain 3 task lists', async () => {
    const fixture = TestBed.createComponent(TasksComponent);
    const component = fixture.componentInstance;
    await component.ngOnInit();
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelectorAll('div.row div.col-4 div.card.rounded-0').length).toBe(3);
  });
});

const tasks = [{
  id: 1,
  title: 'Task 1',
  description: 'Task 1 description',
  status: TaskStatus.New
}, {
  id: 2,
  title: 'Task 2',
  description: 'Task 2 description',
  status: TaskStatus.InProgress
}, {
  id: 2,
  title: 'Task 3',
  description: 'Task 3 description',
  status: TaskStatus.Done
}, {
  id: 2,
  title: 'Task 3',
  description: 'Task 3 description',
  status: TaskStatus.InProgress
}];

class MockTaskService {
  getAll() {
    return new Promise((resolve) => {
      resolve(tasks);
    });
  }
}