import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import TaskService from 'src/services/task-service';
import { Task } from "src/models/task";
import { TaskStatus } from 'src/enums/task-status';

@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html'
})
export class TasksComponent implements OnInit {
  loading: boolean = true;
  tasks: Task[] = [];
  selectedTask: Task | null = null;

  constructor(private router: Router, private taskService: TaskService) {
  }

  async ngOnInit() {
    this.tasks = await this.taskService.getAll();

    this.loading = false;
  }

  get newTasks() {
    return this.tasks.filter(task => task.status == TaskStatus.New);
  }

  get inProgressTasks() {
    return this.tasks.filter(task => task.status == TaskStatus.InProgress);
  }

  get doneTasks() {
    return this.tasks.filter(task => task.status == TaskStatus.Done);
  }

  trackById(index: number, task: Task) {
    return task.id;
  }

  viewTask(task: Task) {
    this.selectedTask = task;
  }

  closeTaskDetailPanel() {
    this.selectedTask = null;
  }

  addNewTask() {
    this.router.navigate(['/tasks/create']);
  }
}
