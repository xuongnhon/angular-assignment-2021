import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Task } from "src/models/task";
import TaskService from 'src/services/task-service';
import { NgForm } from '@angular/forms';
import { TaskStatus } from 'src/enums/task-status';
import { IDeactivateComponent } from 'src/guards/deactivate.guard';

@Component({
  selector: 'edit-task',
  templateUrl: './edit-task.component.html'
})
export class EditTaskComponent implements IDeactivateComponent {
  loading: boolean = true;
  task: Task = {
    id: 0,
    title: '',
    description: '',
    status: TaskStatus.New,
  };

  skipDirtyCheck: boolean = false;
  @ViewChild('taskForm') taskForm: NgForm;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private taskService: TaskService) {
  }

  async ngOnInit() {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.task = await this.taskService.getById(id);

    this.loading = false;
  }

  canExit() {
    if (!this.skipDirtyCheck && (this.taskForm.touched || this.taskForm.dirty)) {
      return confirm("There is unsaved data on this page. Do you wish to continute?");
    }

    return true;
  }

  back() {
    this.router.navigate(['/tasks']);
  }

  async onSubmit() {
    if (this.taskForm.valid) {
      await this.taskService.edit(this.task);

      this.skipDirtyCheck = true;
      this.router.navigate(['/tasks']);
    }
  }

  async delete() {
    const continute = confirm('Are you sure you want to delete?');

    if (continute) {
      await this.taskService.delete(this.task.id);

      this.skipDirtyCheck = true;
      this.router.navigate(['/tasks']);
    }
  }
}
