import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router'
import { NgForm } from '@angular/forms';
import { Task } from 'src/models/task';
import { TaskStatus } from 'src/enums/task-status';
import TaskService from 'src/services/task-service';
import { IDeactivateComponent } from 'src/guards/deactivate.guard';

@Component({
  selector: 'create-task',
  templateUrl: './create-task.component.html'
})
export class CreateTaskComponent implements IDeactivateComponent {
  task: Task = {
    id: 0,
    title: '',
    description: '',
    status: TaskStatus.New
  };

  skipDirtyCheck: boolean = false;
  @ViewChild('taskForm') taskForm: NgForm;

  constructor(private router: Router, private taskService: TaskService) {
  }

  canExit() {
    if (!this.skipDirtyCheck && (this.taskForm.touched || this.taskForm.dirty)) {
      return confirm("There is unsaved data on this page. Do you wish to continute?");
    }

    return true;
  }

  back() {
    this.router.navigate(['/tasks']);
  }

  async onSubmit() {
    if (this.taskForm.valid) {
      await this.taskService.add(this.task);

      this.skipDirtyCheck = true;
      this.router.navigate(['/tasks']);
    }
  }
}
