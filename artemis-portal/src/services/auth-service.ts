import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { UserManager } from 'oidc-client';

const config = {
  authority: environment.authorityPath,
  client_id: environment.clientId,

  redirect_uri: `${window.location.origin}/signin-callback`,
  post_logout_redirect_uri: `${window.location.origin}/signout-callback`,
  silent_redirect_uri: `${window.location.origin}/silent`,

  response_type: "code",

  scope: "openid profile Demeter.Api",
  filterProtocolClaims: true,
  loadUserInfo: true,

  automaticSilentRenew: true,
  revokeAccessTokenOnSignout: true,
};

const oidcManager = new UserManager(config);

@Injectable()
export default class AuthService {
  async getUser() {
    return await oidcManager.getUser();
  }
  async getAccessToken() {
    let user = await this.getUser();

    if (user) {
      return user.access_token;
    }

    return null;
  }
  async isAuthenticated() {
    let user = await this.getUser();

    if (user) {
      return !user.expired;
    }

    return false;
  }
  login() {
    return oidcManager.signinRedirect();
  }
  logout() {
    return oidcManager.signoutRedirect();
  }
  async signinRedirectCallback() {
    return await oidcManager.signinRedirectCallback();
  }
  async signinSilentCallback() {
    return await oidcManager.signinSilentCallback();
  }
  async signoutRedirectCallback() {
    return await oidcManager.signoutRedirectCallback();
  }
}
