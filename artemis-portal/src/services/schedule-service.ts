import { Injectable } from "@angular/core";
import HttpFactory from "src/factories/http-factory";
import { Schedule } from "src/models/schedule";

@Injectable()
export default class ScheduleService {
    constructor(private httpFactory: HttpFactory) {
    }

    async getAll(): Promise<Schedule[]> {
        let axiosInstance = await this.httpFactory.getInstance();

        return (await axiosInstance.get<Schedule[]>("schedules")).data;
    }
    async getById(id: number) {
        let axiosInstance = await this.httpFactory.getInstance();

        return (await axiosInstance.get<Schedule>(`schedules/${id}`)).data;
    }
    async add(schedule: Schedule) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.post("schedules/add", {
            title: schedule.title,
            creatorId: schedule.creator.id,
            description: schedule.description,
            location: schedule.location,
            startDate: schedule.startDate,
            endDate: schedule.endDate
        });
    }
    async edit(schedule: Schedule) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.put("schedules/edit", {
            id: schedule.id,
            title: schedule.title,
            creatorId: schedule.creator.id,
            description: schedule.description,
            location: schedule.location,
            startDate: schedule.startDate,
            endDate: schedule.endDate
        });
    }
    async delete(id: number) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.delete(`schedules/delete/${id}`);
    }
}
