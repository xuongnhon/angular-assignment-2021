import { Injectable } from "@angular/core";
import HttpFactory from "src/factories/http-factory";
import { Task } from "src/models/task";

@Injectable()
export default class TaskService {
    constructor(private httpFactory: HttpFactory) {
    }

    async getAll(): Promise<Task[]> {
        let axiosInstance = await this.httpFactory.getInstance();

        return (await axiosInstance.get<Task[]>("tasks")).data;
    }
    async getById(id: number) {
        let axiosInstance = await this.httpFactory.getInstance();

        return (await axiosInstance.get<Task>(`tasks/${id}`)).data;
    }
    async add(task: Task) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.post("tasks/add", task);
    }
    async edit(task: Task) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.put("tasks/edit", task);
    }
    async delete(id: number) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.delete(`tasks/delete/${id}`);
    }
}
