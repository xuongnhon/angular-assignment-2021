import { Injectable } from "@angular/core";
import HttpFactory from "src/factories/http-factory";
import { User } from "src/models/user";

@Injectable()
export default class UserService {
    constructor(private httpFactory: HttpFactory) {
    }

    async getAll(): Promise<User[]> {
        let axiosInstance = await this.httpFactory.getInstance();

        return (await axiosInstance.get<User[]>("users")).data;
    }
    async getById(id: number) {
        let axiosInstance = await this.httpFactory.getInstance();

        return (await axiosInstance.get<User>(`users/${id}`)).data;
    }
    async add(user: User) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.post("users/add", user);
    }
    async edit(user: User) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.put("users/edit", user);
    }
    async delete(id: number) {
        let axiosInstance = await this.httpFactory.getInstance();

        return await axiosInstance.delete(`users/delete/${id}`);
    }
}
