import { Injectable } from "@angular/core";
import axios from "axios";
import { environment } from "../environments/environment";
import AuthService from "../services/auth-service";

@Injectable()
export default class HttpFactory {
    constructor(private authService: AuthService) {
    }

    async getInstance() {
        let axiosInstance = axios.create({
            baseURL: environment.apiUrl,
        });

        axiosInstance.interceptors.request.use(
            async (config) => {
                config.headers["Pragma"] = "no-cache";
                const accessToken = await this.authService.getAccessToken();
                if (accessToken) {
                    config.headers.common["Authorization"] = `Bearer ${accessToken}`;
                }

                return config;
            },
            (error) => {
                return Promise.reject(error);
            }
        );

        axiosInstance.interceptors.response.use(
            (response) => response,
            (error) => {
                return Promise.reject(error);
            }
        );

        return axiosInstance;
    }
}
