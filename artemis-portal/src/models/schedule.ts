import { User } from "./user";

export class Schedule {
    id: number;
    title: string;
    creator: User;
    description: string;
    location: string;
    startDate: Date | null;
    endDate: Date | null;
}
