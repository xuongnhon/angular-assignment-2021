import { TaskStatus } from "src/enums/task-status";

export class Task {
    id: number;
    title: string;
    description: string;
    status: number = TaskStatus.New;
}
