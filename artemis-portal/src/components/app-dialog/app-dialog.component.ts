import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './app-dialog.component.html'
})
export class DialogComponent {
  @Input() show: boolean = false;
  @Output() closeEvent = new EventEmitter();

  closeDialog(){
    this.closeEvent.emit();
  }
}
