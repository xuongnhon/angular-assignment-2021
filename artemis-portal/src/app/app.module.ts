import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PublicLayoutComponent } from '../views/layouts/public-layout.component';
import { InternalLayoutComponent } from '../views/layouts/internal-layout.component';
import { DialogComponent } from 'src/components/app-dialog/app-dialog.component';
import { LoadingComponent } from 'src/components/loading/loading.component';

import { AccessDeniedComponent } from '../views/auth/access-denied.component';
import { LoginComponent } from '../views/auth/login.component';
import { LogoutComponent } from '../views/auth/logout.component';
import { SigninCallbackComponent } from '../views/auth/signin-callback.component';
import { SignoutCallbackComponent } from '../views/auth/signout-callback.component';
import { SlientComponent } from '../views/auth/slient.component';
import { PageNotFoundComponent } from '../views/page-not-found/page-not-found.component';

import { DashboardComponent } from '../views/dashboard/dashboard.component';
import { TasksComponent } from '../views/tasks/tasks.component';
import { CreateTaskComponent } from '../views/tasks/create-task.component';
import { EditTaskComponent } from '../views/tasks/edit-task.component';
import { UsersComponent } from '../views/users/users.component';
import { UserDetailComponent } from '../views/users/user-detail.component';
import { CreateUserComponent } from '../views/users/create-user.component';
import { CreateUserScheduleComponent } from '../views/users/create-user-schedule.component';
import { SchedulesComponent } from '../views/schedules/schedules.component';
import { EditScheduleComponent } from '../views/schedules/edit-schedule.component';

import { StartDateValidatorDirective } from '../directives/start-date-validator.directive';

import AuthGuard from '../guards/auth.guard';
import DeactivateGuard from '../guards/deactivate.guard';

import HttpFactory from 'src/factories/http-factory';
import AuthService from "../services/auth-service";
import TaskService from 'src/services/task-service';
import UserService from 'src/services/user-service';
import ScheduleService from 'src/services/schedule-service';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full', },
  {
    path: '',
    component: PublicLayoutComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'access-denied', component: AccessDeniedComponent },
      { path: 'login', component: LoginComponent },
      { path: 'logout', component: LogoutComponent },
      { path: 'signin-callback', component: SigninCallbackComponent },
      { path: 'signout-callback', component: SignoutCallbackComponent },
      { path: 'silent', component: SlientComponent },
      { path: 'page-not-found', component: PageNotFoundComponent }
    ]
  },
  {
    path: '',
    component: InternalLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'tasks', component: TasksComponent },
      { path: 'tasks/create', component: CreateTaskComponent, canDeactivate: [DeactivateGuard] },
      { path: 'tasks/edit/:id', component: EditTaskComponent, canDeactivate: [DeactivateGuard] },
      { path: 'users', component: UsersComponent, canDeactivate: [DeactivateGuard] },
      { path: 'users/create', component: CreateUserComponent, canDeactivate: [DeactivateGuard] },
      { path: 'users/:userId/create-schedule', component: CreateUserScheduleComponent, canDeactivate: [DeactivateGuard] },
      { path: 'schedules', component: SchedulesComponent },
      { path: 'schedules/edit/:id', component: EditScheduleComponent, canDeactivate: [DeactivateGuard] }
    ]
  },
  { path: '**', redirectTo: '/page-not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    PublicLayoutComponent,
    InternalLayoutComponent,
    DialogComponent,
    LoadingComponent,

    AccessDeniedComponent,
    LoginComponent,
    LogoutComponent,
    SigninCallbackComponent,
    SignoutCallbackComponent,
    PageNotFoundComponent,

    DashboardComponent,
    TasksComponent,
    CreateTaskComponent,
    EditTaskComponent,
    UsersComponent,
    UserDetailComponent,
    CreateUserComponent,
    CreateUserScheduleComponent,
    SchedulesComponent,
    EditScheduleComponent,

    StartDateValidatorDirective,
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [AuthGuard, DeactivateGuard, HttpFactory, AuthService, TaskService, UserService, ScheduleService],
  bootstrap: [AppComponent],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
