import { Injectable } from "@angular/core";
import { CanActivate, Router, UrlTree } from "@angular/router";
import AuthService from "../services/auth-service";

@Injectable()
export default class AuthGuard implements CanActivate {
    constructor(private router: Router, private authService: AuthService) {
    }

    async canActivate() {
        const isAuthenticated = await this.authService.isAuthenticated();
        if (!isAuthenticated) {
            return this.router.navigate(['/login']);
        }
        return isAuthenticated;
    }
}
