import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

export interface IDeactivateComponent {
    canExit: () => Promise<boolean> | boolean;
}

@Injectable()
export default class DeactivateGuard implements CanDeactivate<IDeactivateComponent> {
    canDeactivate(
        component: IDeactivateComponent
    ): Promise<boolean> | boolean {
        return component.canExit ? component.canExit() : true;
    }
}
