using Demeter.Api.Filters;
using Demeter.Api.Infrastructure.Context;
using Demeter.Api.Infrastructure.Extensions.StartupConfigurations;
using FluentValidation.AspNetCore;
using IdentityModel.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;

namespace Demeter.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AllowAllPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = "Bearer";
                    options.DefaultChallengeScheme = "Bearer";
                })
                .AddIdentityServerAuthentication("Bearer",
                    jwtOptions =>
                    {
                        jwtOptions.Authority = Configuration.GetValue<string>("IdentityUrl");
                        jwtOptions.RequireHttpsMetadata = false;
                        jwtOptions.Audience = "Demeter.Api";
                    },
                    referenceOptions =>
                    {
                        referenceOptions.Authority = Configuration.GetValue<string>("IdentityUrl");
                        referenceOptions.DiscoveryPolicy = new DiscoveryPolicy
                        {
                            Authority = Configuration.GetValue<string>("IdentityUrl"),
                            ValidateIssuerName = false,
                            RequireHttps = false
                        };
                        referenceOptions.ClientId = "Demeter.Api";
                        referenceOptions.ClientSecret = "e579879b-ad1f-4e6f-a73e-0eb452b6819c";
                    }
                );

            services.AddDbContext<DemeterDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddOptions();

            services.AddControllers(config =>
            {
                config.Filters.Add(typeof(RequestValidationFilter));
            })
            .AddFluentValidation(config => config.RegisterValidatorsFromAssemblyContaining<Startup>())
            .AddNewtonsoftJson(options =>
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

            services.AddHttpContextAccessor();

            services.RegisterApplicationDependencyInjection();

            services.AddRouting(options => options.LowercaseUrls = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCustomExceptionHandler(env.IsProduction());

            app.UseCors("AllowAllPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
