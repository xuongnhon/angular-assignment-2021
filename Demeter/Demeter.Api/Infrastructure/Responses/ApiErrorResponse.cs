﻿using Demeter.Api.Models;
using System.Collections.Generic;

namespace Demeter.Api.Infrastructure.Responses
{
    public class ApiErrorResponse
    {
        public int HttpCode { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorCode { get; set; }

        public string CorrelationId { get; set; }

        public string StackTrace { get; set; }

        public IList<ErrorDetail> Details { get; set; }
    }
}
