﻿using Demeter.Api.Common.Constants;
using Demeter.Api.Infrastructure.Exceptions;
using Demeter.Api.Infrastructure.Responses;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Threading.Tasks;

namespace Demeter.Api.Infrastructure.Extensions.StartupConfigurations
{
    public static class ExceptionHandlerExtension
    {
        public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder app, bool isProduction)
        {
            app.UseExceptionHandler(errorConfigure =>
            {
                errorConfigure.Run(async context =>
                {
                    var logger = errorConfigure.ApplicationServices.GetRequiredService<ILogger<Startup>>();
                    logger.LogInformation("Handle exception on request: {context.TraceIdentifier}", context.TraceIdentifier);
                    var exceptionPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();

                    logger.LogError($"Exception caught message: {exceptionPathFeature?.Error.ToString()}");

                    logger.LogError("Create error response with {@exception}", exceptionPathFeature?.Error);
                    var error = CreateErrorResponse(context, exceptionPathFeature?.Error, isProduction);

                    await WriteErrorMessageToResponse(context, error, logger);
                    logger.LogInformation("Finish handle exception on request: {context.TraceIdentifier}", context.TraceIdentifier);
                });
            });

            return app;
        }

        private static ApiErrorResponse CreateErrorResponse(HttpContext context, Exception exception, bool isProduction)
        {
            var errorResponse = new ApiErrorResponse
            {
                CorrelationId = isProduction ? null : context.TraceIdentifier,
                StackTrace = isProduction ? null : exception.StackTrace
            };

            switch (exception)
            {
                case ValidationException v:
                    errorResponse.HttpCode = SupportedHttpCode.BAD_REQUEST;
                    errorResponse.ErrorMessage = GeneralErrorMessage.BAD_REQUEST;
                    errorResponse.Details = v.Details;
                    errorResponse.ErrorCode = v.Error.ErrorCode;
                    break;
                case ApiException a:
                    errorResponse.HttpCode = a.Error.StatusCode;
                    errorResponse.ErrorMessage = a.Error.ErrorMessage;
                    break;
                default:
                    errorResponse.HttpCode = SupportedHttpCode.INTERNAL_SERVER_ERROR;
                    errorResponse.ErrorMessage = GeneralErrorMessage.INTERNAL_SERVER_ERROR;
                    break;
            }

            return errorResponse;
        }

        private static async Task WriteErrorMessageToResponse(HttpContext context, ApiErrorResponse error, ILogger logger)
        {
            logger.LogInformation("Handle Write error messages to response");
            context.Response.StatusCode = error.HttpCode;

            try
            {
                var jsonError = JsonConvert.SerializeObject(error, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                context.Response.ContentType = "application/json";

                logger.LogInformation("Write error messages to response with json {jsonError}", jsonError);
                await context.Response.WriteAsync(jsonError).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                logger.LogError($"Exception caught message: {ex.ToString()}");
            }
        }
    }
}
