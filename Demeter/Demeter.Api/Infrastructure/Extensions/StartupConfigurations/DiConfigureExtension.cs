﻿using Demeter.Api.Domain.ServiceContracts;
using Demeter.Api.Infrastructure.Authorization;
using Demeter.Api.Infrastructure.DataAccess;
using Demeter.Api.Infrastructure.Utils;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Demeter.Api.Infrastructure.Extensions.StartupConfigurations
{
    public static class DiConfigureExtension
    {
        public static IServiceCollection RegisterApplicationDependencyInjection(this IServiceCollection services)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var domainServices = TypeHelper.FindClassesOfType<IBaseService>(assemblies);
            foreach (var domainService in domainServices)
            {
                var domainServiceInterface = domainService.GetInterfaces().First();
                services.AddScoped(domainServiceInterface, domainService);
            }

            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));

            return services;
        }
    }
}
