﻿using Demeter.Api.Common.Constants;
using Demeter.Api.Models;
using System;
using System.Runtime.Serialization;

namespace Demeter.Api.Infrastructure.Exceptions
{
    [Serializable]
    public class ApiException : Exception
    {
        public ApiException(string message) : base(message, null)
        {

        }

        protected ApiException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {

        }

        public ApiException(BaseError error, Exception inner)
            : base(error.ErrorMessage, inner)
        {
            Error = error;
        }

        public ApiException(string message, Exception inner) : base(message, inner)
        {
            Error.StatusCode = SupportedHttpCode.INTERNAL_SERVER_ERROR;
        }

        public BaseError Error { get; set; } = new BaseError();
    }
}
