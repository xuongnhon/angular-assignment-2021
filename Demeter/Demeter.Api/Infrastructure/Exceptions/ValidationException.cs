﻿using Demeter.Api.Models;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Demeter.Api.Infrastructure.Exceptions
{
    [Serializable]
    public class ValidationException : ApiException
    {
        public IList<ErrorDetail> Details { get; set; }

        protected ValidationException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {

        }

        public ValidationException(string errorCode, IList<ErrorDetail> details) : base("Validation exception")
        {
            Error.ErrorCode = errorCode;
            Details = details;
        }

        public ValidationException(string errorCode) : base("Validation exception")
        {
            Error.ErrorCode = errorCode;
        }

        public ValidationException(IList<ErrorDetail> details) : base("Validation exception")
        {
            Details = details;
        }
    }
}
