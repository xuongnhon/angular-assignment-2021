﻿namespace Demeter.Api.Models
{
    public class BaseError
    {
        public int StatusCode { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorCode { get; set; }
    }
}
