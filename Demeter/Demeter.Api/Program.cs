using Demeter.Api.Infrastructure.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace Demeter.Api
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            MigrateDatabase(host.Services);

            host.Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                    webBuilder.UseContentRoot(Directory.GetCurrentDirectory());

                    webBuilder.ConfigureAppConfiguration((hostContext, options) =>
                    {
                        options.AddJsonFile("appsettings.json", true, true);

                        if (hostContext.HostingEnvironment.IsEnvironment("Debug"))
                        {
                            options.AddUserSecrets<Startup>();
                        }

                        options.AddEnvironmentVariables();
                        options.AddCommandLine(args);
                    });
                });

        private static void MigrateDatabase(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetService<DemeterDbContext>();
                context.Database.Migrate();
            }
        }
    }
}
