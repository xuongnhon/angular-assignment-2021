﻿using System;

namespace Demeter.Api.Requests
{
    public class AddScheduleRequest
    {
        public string Title { get; set; }

        public int CreatorId { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
