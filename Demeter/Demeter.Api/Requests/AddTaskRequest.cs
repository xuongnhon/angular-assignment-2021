﻿using Demeter.Api.Domain.Common.Enum;

namespace Demeter.Api.Requests
{
    public class AddTaskRequest
    {
        public string Title { get; set; }

        public TaskStatus Status { get; set; }

        public string Description { get; set; }
    }
}
