﻿namespace Demeter.Api.Requests
{
    public class EditUserRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Role { get; set; }
    }
}
