﻿namespace Demeter.Api.Requests
{
    public class AddUserRequest
    {
        public string Name { get; set; }

        public string Role { get; set; }
    }
}
