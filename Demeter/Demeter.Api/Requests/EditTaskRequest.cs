﻿using Demeter.Api.Domain.Common.Enum;

namespace Demeter.Api.Requests
{
    public class EditTaskRequest
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public TaskStatus Status { get; set; }

        public string Description { get; set; }
    }
}
