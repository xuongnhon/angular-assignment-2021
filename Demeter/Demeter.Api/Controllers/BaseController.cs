﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demeter.Api.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {

    }
}
