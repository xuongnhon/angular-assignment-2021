﻿using Demeter.Api.Domain.Dto;
using Demeter.Api.Domain.ServiceContracts;
using Demeter.Api.Requests;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Demeter.Api.Controllers
{
    public class TaskController : BaseController
    {
        private readonly ITaskService _taskService;

        public TaskController(
            ITaskService taskService
        )
        {
            _taskService = taskService;
        }

        [HttpGet("~/api/tasks")]
        public async Task<IActionResult> Get()
        {
            var tasks = await _taskService.GetAll();

            return Ok(tasks);
        }

        [HttpGet("~/api/tasks/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _taskService.GetById(id);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);
        }

        [HttpPost("~/api/tasks/add")]
        public async Task<IActionResult> Add([FromBody] AddTaskRequest request)
        {
            var result = await _taskService.Create(new TaskDto
            {
                Title = request.Title,
                Description = request.Description,
                Status = request.Status
            });

            return Ok(result);
        }

        [HttpPut("~/api/tasks/edit")]
        public async Task<IActionResult> Edit([FromBody] EditTaskRequest request)
        {
            var result = await _taskService.Edit(new TaskDto
            {
                Id = request.Id,
                Title = request.Title,
                Description = request.Description,
                Status = request.Status
            });

            return Ok(result);
        }

        [HttpDelete("~/api/tasks/delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _taskService.Delete(id);

            return Ok(result);
        }
    }
}
