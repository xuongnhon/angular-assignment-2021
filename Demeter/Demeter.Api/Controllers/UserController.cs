﻿using Demeter.Api.Domain.Dto;
using Demeter.Api.Domain.ServiceContracts;
using Demeter.Api.Requests;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Demeter.Api.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(
            IUserService userService
        )
        {
            _userService = userService;
        }

        [HttpGet("~/api/users")]
        public async Task<IActionResult> Get()
        {
            var Users = await _userService.GetAll();

            return Ok(Users);
        }

        [HttpGet("~/api/users/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _userService.GetById(id);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);
        }

        [HttpPost("~/api/users/add")]
        public async Task<IActionResult> Add([FromBody] AddUserRequest request)
        {
            var result = await _userService.Create(new UserDto
            {
                Name = request.Name,
                Role = request.Role
            });

            return Ok(result);
        }

        [HttpPut("~/api/users/edit")]
        public async Task<IActionResult> Edit([FromBody] EditUserRequest request)
        {
            var result = await _userService.Edit(new UserDto
            {
                Id = request.Id,
                Name = request.Name,
                Role = request.Role
            });

            return Ok(result);
        }

        [HttpDelete("~/api/users/delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _userService.Delete(id);

            return Ok(result);
        }
    }
}
