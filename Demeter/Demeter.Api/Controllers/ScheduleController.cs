﻿using Demeter.Api.Domain.Dto;
using Demeter.Api.Domain.ServiceContracts;
using Demeter.Api.Requests;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Demeter.Api.Controllers
{
    public class ScheduleController : BaseController
    {
        private readonly IScheduleService _scheduleService;

        public ScheduleController(
            IScheduleService scheduleService
        )
        {
            _scheduleService = scheduleService;
        }

        [HttpGet("~/api/schedules")]
        public async Task<IActionResult> Get()
        {
            var Schedules = await _scheduleService.GetAll();

            return Ok(Schedules);
        }

        [HttpGet("~/api/schedules/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _scheduleService.GetById(id);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);
        }

        [HttpPost("~/api/schedules/add")]
        public async Task<IActionResult> Add([FromBody] AddScheduleRequest request)
        {
            var result = await _scheduleService.Create(new ScheduleDto
            {
                Title = request.Title,
                Creator = new UserDto { 
                    Id = request.CreatorId
                },
                Description = request.Description,
                Location = request.Location,
                StartDate = request.StartDate,
                EndDate = request.EndDate
            });

            return Ok(result);
        }

        [HttpPut("~/api/schedules/edit")]
        public async Task<IActionResult> Edit([FromBody] EditScheduleRequest request)
        {
            var result = await _scheduleService.Edit(new ScheduleDto
            {
                Id = request.Id,
                Title = request.Title,
                Creator = new UserDto
                {
                    Id = request.CreatorId
                },
                Description = request.Description,
                Location = request.Location,
                StartDate = request.StartDate,
                EndDate = request.EndDate
            });

            return Ok(result);
        }

        [HttpDelete("~/api/schedules/delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _scheduleService.Delete(id);

            return Ok(result);
        }
    }
}
