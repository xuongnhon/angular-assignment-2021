﻿using Demeter.Api.Common.Constants;
using FluentValidation;
using System;
using System.Linq;

namespace Demeter.Api.Validations.Common
{
    public static class EnumValidator
    {
        private const int UNKNOWN_VALUE = 0;

        public static bool IsValidAndNotUnknown<T>(int enumValue)
        {
            var enumTypeRange = Enum.GetValues(typeof(T)).Cast<int>().ToList();

            return enumValue != UNKNOWN_VALUE && enumTypeRange.Contains(enumValue);
        }

        public static IRuleBuilderOptions<T, TProperty>
            IsEnumValid<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder)
            where TProperty : Enum
        {
            return ruleBuilder.Must(enumValue =>
            {
                return IsValidAndNotUnknown<TProperty>(Convert.ToInt32(enumValue));
            })
            .WithErrorCode(ErrorCode.INVALID_REQUEST);
        }
    }
}
