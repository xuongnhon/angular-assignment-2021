﻿using Demeter.Api.Common.Constants;
using Demeter.Api.Requests;
using FluentValidation;

namespace Demeter.Api.Validations.RequestValidators
{
    public class AddScheduleRequestValidator : AbstractValidator<AddScheduleRequest>
    {
        public AddScheduleRequestValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Title)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.CreatorId)
                .GreaterThan(0)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Description)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Location)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.StartDate)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.EndDate)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m)
                .Must(m =>
                {
                    return m.StartDate < m.EndDate;
                })
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
