﻿using Demeter.Api.Common.Constants;
using Demeter.Api.Requests;
using FluentValidation;

namespace Demeter.Api.Validations.RequestValidators
{
    public class AddUserRequestValidator : AbstractValidator<AddUserRequest>
    {
        public AddUserRequestValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Name)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Role)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
