﻿using Demeter.Api.Common.Constants;
using Demeter.Api.Requests;
using Demeter.Api.Validations.Common;
using FluentValidation;

namespace Demeter.Api.Validations.RequestValidators
{
    public class EditTaskRequestValidator : AbstractValidator<EditTaskRequest>
    {
        public EditTaskRequestValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Id)
                .GreaterThan(0)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Title)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Description)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Status)
                .IsEnumValid();
        }
    }
}
