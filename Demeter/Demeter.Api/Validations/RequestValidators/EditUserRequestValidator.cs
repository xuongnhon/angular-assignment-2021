﻿using Demeter.Api.Common.Constants;
using Demeter.Api.Requests;
using FluentValidation;

namespace Demeter.Api.Validations.RequestValidators
{
    public class EditUserRequestValidator : AbstractValidator<EditUserRequest>
    {
        public EditUserRequestValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Id)
                .GreaterThan(0)
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Name)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);

            RuleFor(m => m.Role)
                .NotNull()
                .WithMessage(ErrorCode.INVALID_REQUEST);
        }
    }
}
