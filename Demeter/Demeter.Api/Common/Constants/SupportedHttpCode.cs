﻿namespace Demeter.Api.Common.Constants
{
    public static class SupportedHttpCode
    {
        public const int BAD_REQUEST = 400;
        public const int INTERNAL_SERVER_ERROR = 500;
    }
}
