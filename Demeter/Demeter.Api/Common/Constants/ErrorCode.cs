﻿namespace Demeter.Api.Common.Constants
{
    public static class ErrorCode
    {
        public const string INVALID_CONFIGURATION = "INVALID_CONFIGURATION";
        public const string INVALID_REQUEST = "INVALID_REQUEST";
        public const string MISSING_REQUIRED_FIELD = "MISSING_REQUIRED_FIELD";
        public const string REQUEST_CANCELLED = "REQUEST_CANCELLED";
    }
}
