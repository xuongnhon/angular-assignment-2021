﻿namespace Demeter.Api.Common.Constants
{
    public static class GeneralErrorMessage
    {
        public const string BAD_REQUEST = "Bad Request";
        public const string INTERNAL_SERVER_ERROR = "Internal Server Error";
    }
}
