﻿using Demeter.Api.Infrastructure.Exceptions;
using Demeter.Api.Models;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demeter.Api.Filters
{
    public class RequestValidationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var modelState = context.ModelState;
            if (!modelState.IsValid)
            {
                var errorDetails = new List<ErrorDetail>();
                foreach (var key in modelState.Keys)
                {
                    errorDetails.Add(new ErrorDetail
                    {
                        Target = key,
                        Message = modelState[key].Errors.FirstOrDefault()?.ErrorMessage
                    });
                }

                throw new ValidationException(errorDetails);
            }

            await next();
        }
    }
}
