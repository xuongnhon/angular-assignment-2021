﻿using Demeter.Api.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demeter.Api.Domain.ServiceContracts
{
    public interface IUserService : IBaseService
    {
        Task<List<UserDto>> GetAll();
        Task<UserDto> GetById(int id);
        Task<ResultDto> Create(UserDto User);
        Task<ResultDto> Edit(UserDto User);
        Task<ResultDto> Delete(int id);
    }
}
