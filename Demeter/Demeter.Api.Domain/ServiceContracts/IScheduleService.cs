﻿using Demeter.Api.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demeter.Api.Domain.ServiceContracts
{
    public interface IScheduleService : IBaseService
    {
        Task<List<ScheduleDto>> GetAll();
        Task<ScheduleDto> GetById(int id);
        Task<ResultDto> Create(ScheduleDto Schedule);
        Task<ResultDto> Edit(ScheduleDto Schedule);
        Task<ResultDto> Delete(int id);
    }
}
