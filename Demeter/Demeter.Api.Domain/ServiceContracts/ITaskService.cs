﻿using Demeter.Api.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demeter.Api.Domain.ServiceContracts
{
    public interface ITaskService : IBaseService
    {
        Task<List<TaskDto>> GetAll();
        Task<TaskDto> GetById(int id);
        Task<ResultDto> Create(TaskDto task);
        Task<ResultDto> Edit(TaskDto task);
        Task<ResultDto> Delete(int id);
    }
}
