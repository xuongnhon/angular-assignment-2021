﻿using Demeter.Api.Domain.Common.Constants;
using Demeter.Api.Domain.Common.Enum;
using Demeter.Api.Domain.Dto;
using Demeter.Api.Domain.ServiceContracts;
using Demeter.Api.Infrastructure.DataAccess;
using Demeter.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demeter.Api.Domain.Services
{
    public class ScheduleService : IScheduleService
    {
        private readonly IRepository<ScheduleEntity, int> _scheduleRepository;
        private readonly IUnitOfWork _uow;

        public ScheduleService(
            IRepository<ScheduleEntity, int> scheduleRepository,
            IUnitOfWork uow
        )
        {
            _scheduleRepository = scheduleRepository;
            _uow = uow;
        }

        public async Task<List<ScheduleDto>> GetAll()
        {
            var result = await _scheduleRepository.DbSet
                .Select(schedule => new ScheduleDto
                {
                    Id = schedule.Id,
                    Title = schedule.Title,
                    Creator = new UserDto {
                        Id = schedule.Creator.Id,
                        Name = schedule.Creator.Name,
                        Role = schedule.Creator.Role
                    },
                    Description = schedule.Description,
                    Location = schedule.Location,
                    StartDate = schedule.StartDate,
                    EndDate = schedule.EndDate
                })
                .ToListAsync();

            return result;
        }

        public async Task<ScheduleDto> GetById(int id)
        {
            var result = await _scheduleRepository.DbSet
                .Where(schedule => schedule.Id == id)
                .Select(schedule => new ScheduleDto
                {
                    Id = schedule.Id,
                    Title = schedule.Title,
                    Creator = new UserDto
                    {
                        Id = schedule.Creator.Id,
                        Name = schedule.Creator.Name,
                        Role = schedule.Creator.Role
                    },
                    Description = schedule.Description,
                    Location = schedule.Location,
                    StartDate = schedule.StartDate,
                    EndDate = schedule.EndDate
                })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ResultDto> Create(ScheduleDto schedule)
        {
            var newSchedule = new ScheduleEntity
            {
                Title = schedule.Title,
                CreatorId = schedule.Creator.Id,
                Description = schedule.Description,
                Location = schedule.Location,
                StartDate = schedule.StartDate,
                EndDate = schedule.EndDate
            };

            _scheduleRepository.Add(newSchedule);

            await _uow.CommitChangesAsync();

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Message.CreateScheduleSuccessfully,
                Data = newSchedule.Id
            };
        }

        public async Task<ResultDto> Edit(ScheduleDto schedule)
        {
            var dbSchedule = await _scheduleRepository.FindOneAsync(x => x.Id == schedule.Id);
            if (dbSchedule != null)
            {
                dbSchedule.Id = schedule.Id;
                dbSchedule.Title = schedule.Title;
                dbSchedule.CreatorId = schedule.Creator.Id;
                dbSchedule.Description = schedule.Description;
                dbSchedule.Location = schedule.Location;
                dbSchedule.StartDate = schedule.StartDate;
                dbSchedule.EndDate = schedule.EndDate;

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.EditScheduleSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.ScheduleNotFound
            };
        }

        public async Task<ResultDto> Delete(int id)
        {
            var Schedule = await _scheduleRepository.FindOneAsync(x => x.Id == id);
            if (Schedule != null)
            {
                _scheduleRepository.Delete(Schedule);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.DeleteScheduleSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.ScheduleNotFound
            };
        }
    }
}
