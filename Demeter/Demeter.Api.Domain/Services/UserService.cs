﻿using Demeter.Api.Domain.Common.Constants;
using Demeter.Api.Domain.Common.Enum;
using Demeter.Api.Domain.Dto;
using Demeter.Api.Domain.ServiceContracts;
using Demeter.Api.Infrastructure.DataAccess;
using Demeter.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demeter.Api.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<UserEntity, int> _userRepository;
        private readonly IUnitOfWork _uow;

        public UserService(
            IRepository<UserEntity, int> userRepository,
            IUnitOfWork uow
        )
        {
            _userRepository = userRepository;
            _uow = uow;
        }

        public async Task<List<UserDto>> GetAll()
        {
            var result = await _userRepository.DbSet
                .Select(user => new UserDto
                {
                    Id = user.Id,
                    Name = user.Name,
                    Role = user.Role
                })
                .ToListAsync();

            return result;
        }

        public async Task<UserDto> GetById(int id)
        {
            var result = await _userRepository.DbSet
                .Where(user => user.Id == id)
                .Select(user => new UserDto
                {
                    Id = user.Id,
                    Name = user.Name,
                    Role = user.Role
                })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ResultDto> Create(UserDto user)
        {
            var newUser = new UserEntity
            {
                Name = user.Name,
                Role = user.Role
            };

            _userRepository.Add(newUser);

            await _uow.CommitChangesAsync();

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Message.CreateUserSuccessfully,
                Data = newUser.Id
            };
        }

        public async Task<ResultDto> Edit(UserDto user)
        {
            var dbUser = await _userRepository.FindOneAsync(x => x.Id == user.Id);
            if (dbUser != null)
            {
                dbUser.Name = user.Name;
                dbUser.Role = user.Role;

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.EditUserSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.UserNotFound
            };
        }

        public async Task<ResultDto> Delete(int id)
        {
            var User = await _userRepository.FindOneAsync(x => x.Id == id);
            if (User != null)
            {
                _userRepository.Delete(User);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.DeleteUserSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.UserNotFound
            };
        }
    }
}
