﻿using Demeter.Api.Domain.Common.Constants;
using Demeter.Api.Domain.Common.Enum;
using Demeter.Api.Domain.Dto;
using Demeter.Api.Domain.ServiceContracts;
using Demeter.Api.Infrastructure.DataAccess;
using Demeter.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demeter.Api.Domain.Services
{
    public class TaskService : ITaskService
    {
        private readonly IRepository<TaskEntity, int> _taskRepository;
        private readonly IUnitOfWork _uow;

        public TaskService(
            IRepository<TaskEntity, int> taskRepository,
            IUnitOfWork uow
        )
        {
            _taskRepository = taskRepository;
            _uow = uow;
        }

        public async Task<List<TaskDto>> GetAll()
        {
            var result = await _taskRepository.DbSet
                .Select(task => new TaskDto
                {
                    Id = task.Id,
                    Title = task.Title,
                    Description = task.Description,
                    Status = (Common.Enum.TaskStatus)task.Status
                })
                .ToListAsync();

            return result;
        }

        public async Task<TaskDto> GetById(int id)
        {
            var result = await _taskRepository.DbSet
                .Where(task => task.Id == id)
                .Select(task => new TaskDto
                {
                    Id = task.Id,
                    Title = task.Title,
                    Description = task.Description,
                    Status = (Common.Enum.TaskStatus)task.Status
                })
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ResultDto> Create(TaskDto task)
        {
            var newTask = new TaskEntity
            {
                Title = task.Title,
                Description = task.Description,
                Status = (Infrastructure.Common.Enum.TaskStatus)task.Status
            };

            _taskRepository.Add(newTask);

            await _uow.CommitChangesAsync();

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Message.CreateTaskSuccessfully,
                Data = newTask.Id
            };
        }

        public async Task<ResultDto> Edit(TaskDto task)
        {
            var dbTask = await _taskRepository.FindOneAsync(x => x.Id == task.Id);
            if (dbTask != null)
            {
                dbTask.Title = task.Title;
                dbTask.Description = task.Description;
                dbTask.Status = (Infrastructure.Common.Enum.TaskStatus)task.Status;

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.EditTaskSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.TaskNotFound
            };
        }

        public async Task<ResultDto> Delete(int id)
        {
            var task = await _taskRepository.FindOneAsync(x => x.Id == id);
            if (task != null)
            {
                _taskRepository.Delete(task);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Message.DeleteTaskSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Message.TaskNotFound
            };
        }
    }
}
