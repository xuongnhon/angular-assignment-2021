﻿using Demeter.Api.Domain.Common.Enum;

namespace Demeter.Api.Domain.Dto
{
    public class TaskDto : BaseDto<int>
    {
        public string Title { get; set; }

        public TaskStatus Status { get; set; }

        public string Description { get; set; }
    }
}
