﻿using System;

namespace Demeter.Api.Domain.Dto
{
    public class ScheduleDto : BaseDto<int>
    {
        public string Title { get; set; }

        public UserDto Creator { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
