﻿namespace Demeter.Api.Domain.Dto
{
    public class UserDto : BaseDto<int>
    {
        public string Name { get; set; }

        public string Role { get; set; }
    }
}
