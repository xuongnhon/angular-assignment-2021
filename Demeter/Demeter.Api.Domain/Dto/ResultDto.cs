﻿using Demeter.Api.Domain.Common.Enum;

namespace Demeter.Api.Domain.Dto
{
    public class ResultDto
    {
        public ActionStatus Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
