﻿namespace Demeter.Api.Domain.Common.Enum
{
    public enum ActionStatus
    {
        None = 0,
        Success = 1,
        Error = -1
    }
}
