﻿namespace Demeter.Api.Domain.Common.Constants
{
    public static class Message
    {
        public const string ErrorMessage = "Sorry an error has occurred, please try again later.";

        public const string CreateTaskSuccessfully = "Create task successfully!";
        public const string EditTaskSuccessfully = "Edit task successfully!";
        public const string DeleteTaskSuccessfully = "Delete task successfully!";
        public const string TaskNotFound = "Task could not be found!";

        public const string CreateUserSuccessfully = "Create user successfully!";
        public const string EditUserSuccessfully = "Edit user successfully!";
        public const string DeleteUserSuccessfully = "Delete user successfully!";
        public const string UserNotFound = "User could not be found!";

        public const string CreateScheduleSuccessfully = "Create schedule successfully!";
        public const string EditScheduleSuccessfully = "Edit schedule successfully!";
        public const string DeleteScheduleSuccessfully = "Delete schedule successfully!";
        public const string ScheduleNotFound = "Schedule could not be found!";
    }
}
