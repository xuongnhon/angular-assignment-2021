﻿using Demeter.Api.Infrastructure.Common.Enum;
using Demeter.Api.Infrastructure.Context;
using Demeter.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Demeter.Api.Infrastructure.DataAccess
{
    public class Repository<TEntity, TId> : IRepository<TEntity, TId>
        where TEntity : BaseEntity<TId>
    {
        private readonly DemeterDbContext _DemeterDbContext;

        public Repository(DemeterDbContext DemeterDbContext)
        {
            _DemeterDbContext = DemeterDbContext;
        }

        public IQueryable<TEntity> DbSet => _DemeterDbContext.Set<TEntity>();

        public void Add(TEntity entity)
        {
            _DemeterDbContext.Set<TEntity>().Add(entity);
        }

        public void Attach(TEntity entity, EntityState state = EntityState.Unchanged)
        {
            _DemeterDbContext.Set<TEntity>().Attach(entity);
            switch (state)
            {
                case EntityState.Added:
                    _DemeterDbContext.Entry(entity).State = EntityState.Added;
                    break;
                case EntityState.Deleted:
                    _DemeterDbContext.Entry(entity).State = EntityState.Deleted;
                    break;
                case EntityState.Modified:
                    _DemeterDbContext.Entry(entity).State = EntityState.Modified;
                    break;
                default:
                    _DemeterDbContext.Entry(entity).State = EntityState.Unchanged;
                    break;
            }
        }

        public void Update(TEntity entity, Expression<Func<TEntity, bool>> criteria)
        {
            var original = FindOne(criteria);
            _DemeterDbContext.Entry(original).CurrentValues.SetValues(entity);
        }

        public void Delete(TEntity entity)
        {
            _DemeterDbContext.Set<TEntity>().Remove(entity);
        }

        public void Delete(Expression<Func<TEntity, bool>> criteria)
        {
            var records = DbSet.Where(criteria);

            foreach (var record in records)
            {
                Delete(record);
            }
        }

        private IQueryable<TEntity> GetQuery()
        {
            return _DemeterDbContext.Set<TEntity>();
        }

        public IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQuery().Where(predicate);
        }

        public TEntity GetByKey(TId keyValue)
        {
            return FindOne(x => x.Id.Equals(keyValue));
        }

        public Task<TEntity> GetByKeyAsync(TId keyValue)
        {
            return FindOneAsync(x => x.Id.Equals(keyValue));
        }

        public TEntity Single(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Single(criteria);
        }

        public Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().SingleAsync(criteria);
        }

        public TEntity First()
        {
            return GetQuery().First();
        }

        public TEntity First(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQuery().First(predicate);
        }

        public Task<TEntity> FirstAsync()
        {
            return GetQuery().FirstAsync();
        }

        public Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQuery().FirstAsync(predicate);
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Where(criteria);
        }

        public TEntity FindOne(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Where(criteria).FirstOrDefault();
        }

        public Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Where(criteria).FirstOrDefaultAsync();
        }

        public IQueryable<TEntity> GetAll()
        {
            return GetQuery();
        }

        public IQueryable<TEntity> Get<TOrderBy>(Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex,
            int pageSize, OrderType orderType = OrderType.Ascending)
        {
            if (orderType == OrderType.Ascending)
            {
                return GetQuery().OrderBy(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }
            return GetQuery().OrderByDescending(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }

        public IQueryable<TEntity> Get<TOrderBy>(Expression<Func<TEntity, bool>> criteria,
            Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex, int pageSize,
            OrderType orderType = OrderType.Ascending)
        {
            if (orderType == OrderType.Ascending)
            {
                return DbSet.Where(criteria).OrderBy(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }
            return DbSet.Where(criteria).OrderByDescending(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }

        public int Count()
        {
            return GetQuery().Count();
        }

        public int Count(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Count(criteria);
        }

        public Task<int> CountAsync()
        {
            return GetQuery().CountAsync();
        }

        public Task<int> CountAsync(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().CountAsync(criteria);
        }
    }
}
