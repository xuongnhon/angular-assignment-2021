﻿using Demeter.Api.Infrastructure.Context;
using System;
using System.Threading.Tasks;

namespace Demeter.Api.Infrastructure.DataAccess
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool _disposed;
        private readonly DemeterDbContext _DemeterDbContext;

        public UnitOfWork(DemeterDbContext DemeterDbContext)
        {
            _DemeterDbContext = DemeterDbContext;
        }

        public virtual void CommitChanges()
        {
            _DemeterDbContext.SaveChanges();
        }

        public virtual async Task CommitChangesAsync()
        {
            await _DemeterDbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _DemeterDbContext.Dispose();
            }

            _disposed = true;
        }
    }
}
