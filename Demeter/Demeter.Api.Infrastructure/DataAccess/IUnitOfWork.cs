﻿using System.Threading.Tasks;

namespace Demeter.Api.Infrastructure.DataAccess
{
    public interface IUnitOfWork
    {
        void CommitChanges();
        Task CommitChangesAsync();
    }
}
