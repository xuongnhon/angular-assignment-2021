﻿namespace Demeter.Api.Infrastructure.Common.Enum
{
    public enum TaskStatus
    {
        Unknown = 0,
        New = 1,
        InProgress = 2,
        Done = 3
    }
}
