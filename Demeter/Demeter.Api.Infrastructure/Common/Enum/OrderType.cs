﻿namespace Demeter.Api.Infrastructure.Common.Enum
{
    public enum OrderType
    {
        Unknown = 0,
        Ascending = 1,
        Descending = 2
    }
}
