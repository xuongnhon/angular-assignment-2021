﻿using Demeter.Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Demeter.Api.Infrastructure.Context
{
    public class DemeterDbContext : DbContext
    {
        public DemeterDbContext(DbContextOptions<DemeterDbContext> options)
            : base(options)
        {

        }

        public DbSet<TaskEntity> Tasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskEntity>(m =>
            {

            });

            modelBuilder.Entity<UserEntity>(m =>
            {
                m.HasMany(x => x.Schedules).WithOne(x => x.Creator).HasForeignKey(x => x.CreatorId).IsRequired();
            });

            modelBuilder.Entity<ScheduleEntity>(m =>
            {
                m.HasOne(x => x.Creator).WithMany(x => x.Schedules).HasForeignKey(x => x.CreatorId).IsRequired();
            });

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            AddAuditData();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            AddAuditData();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            AddAuditData();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            AddAuditData();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected void AddAuditData()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is BaseEntity<int> && (x.State == EntityState.Added || x.State == EntityState.Modified))
                .ToList();

            if (entities.Count > 0)
            {
                foreach (var entity in entities)
                {
                    if (entity.State == EntityState.Added)
                    {
                        ((BaseEntity<int>)entity.Entity).CreatedDate = DateTime.UtcNow;
                    }
                    else if (entity.State == EntityState.Modified)
                    {
                        ((BaseEntity<int>)entity.Entity).UpdatedDate = DateTime.UtcNow;
                    }
                }
            }
        }
    }
}
