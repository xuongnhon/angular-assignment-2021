﻿using System;

namespace Demeter.Api.Infrastructure.Entities
{
    public class ScheduleEntity : BaseEntity<int>
    {
        public string Title { get; set; }

        public int CreatorId { get; set; }
        public UserEntity Creator { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
