﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Demeter.Api.Infrastructure.Entities
{
    public abstract class BaseEntity<T>
    {
        [Key]
        public T Id { get; set; }

        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

        [Timestamp]
        public byte[] Version { get; set; }
    }
}
