﻿using System.Collections.Generic;

namespace Demeter.Api.Infrastructure.Entities
{
    public class UserEntity : BaseEntity<int>
    {
        public string Name { get; set; }

        public string Role { get; set; }

        public IList<ScheduleEntity> Schedules { get; set; }
    }
}
