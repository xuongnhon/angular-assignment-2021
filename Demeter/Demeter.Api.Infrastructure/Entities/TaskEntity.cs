﻿using Demeter.Api.Infrastructure.Common.Enum;

namespace Demeter.Api.Infrastructure.Entities
{
    public class TaskEntity : BaseEntity<int>
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public TaskStatus Status { get; set; }
    }
}
